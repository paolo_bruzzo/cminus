# Makefile for cminus

CC=gcc
CC1=clang++
LEX=flex
YACC=bison
MV=/bin/mv
RM=/bin/rm -rf
CP=/bin/cp

CFLAGS=-g -c
YFLAGS=-d

PROGRAM=codegen
OBJECTS=cmlexer.o cmparser.o symbolTable.o ast.o util.o typecheck.o codegen.o
SOURCES=cmlexer.l cmparser.y symbolTable.c ast.c util.c typecheck.c codegen.cpp
CTEMPS=cmlexer.c cmparser.c cmparser.tab.h cmparser.tab.c cmparser.output

################# Codegen Executable ######################

$(PROGRAM): $(OBJECTS)
	$(CC1) -g $(OBJECTS) `llvm-config --ldflags` `llvm-config --libs` -ll -ldl -pthread -lpthread -o $(PROGRAM)

codegen.o: codegen.cpp
	$(CC1) $(CFLAGS) `llvm-config --cxxflags` -o codegen.o codegen.cpp

typecheck.o: typecheck.c
	$(CC) $(CFLAGS) typecheck.c

util.o: util.c
	$(CC) $(CFLAGS) util.c

ast.o:  ast.c
	$(CC) $(CFLAGS) ast.c

symbolTable.o: symbolTable.c
	$(CC) $(CFLAGS) symbolTable.c

cmparser.o: cmparser.c 
	$(CC) $(CFLAGS) cmparser.c

cmparser.c: cmparser.y
	$(YACC) cmparser.y -r all -o cmparser.c

cmlexer.o: cmlexer.c cmparser.tab.h globals.h
	$(CC) $(CFLAGS) cmlexer.c

cmparser.tab.h: cmparser.y
	$(YACC) $(YFLAGS) cmparser.y 

cmlexer.c: cmlexer.l 
	$(LEX) -o cmlexer.c cmlexer.l

################# Parser Graph ######################

# 2) Generate the pdf fro the .dot file
graph: cmparser.dot
	dot -T pdf cmparser.dot -o parser_graph.pdf

# 1) Generate the .dot file from the grammar
cmparser.dot: cmparser.y
	$(YACC) -g cmparser.y

################# Clean ######################
clean:
	$(RM) $(PROGRAM) $(OBJECTS) $(CTEMPS) *~ cmparser.dot parser_graph.pdf
