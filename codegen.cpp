#include "llvm/IR/Verifier.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Bitcode/ReaderWriter.h"

#include <cstdio>
#include <map>
#include <string>
#include <vector>

// Where to output file will be saved
#define OUTPUT_FILE_PATH "/home/pbruzz2/6-Code_Generation/"
#define DEFAULT_OUTPUT_FILE_NAME "out.ll"

namespace cmodules {
  extern "C" {
    #include "ast.h"
  }
}

using namespace llvm;

/* =================================================================
                    EXTERNAL FUNCTIONS and VARIABLES
 * ================================================================= */

extern "C" int  initLex(int argc, char** argv);
extern "C" void initSymbolTable();  
extern "C" int  yyparse();
extern "C" int  typecheck();
extern "C" int  gettoken();
extern "C" void print_Ast();
extern "C" void printSymbolTable(); 

extern "C" cmodules::AstNodePtr program;
extern "C" cmodules::SymbolTableStackEntryPtr symbolStackTop;

/* =================================================================
                            VARIABLES
 * ================================================================= */

/* TheModule is The LLVM construct that contains all of The functions and global 
 * variables in a chunk of code. In many ways, it is The top-level structure that 
 * The LLVM IR uses to contain code. */
static Module *TheModule;

/* Instances of The IRBuilder class template keep track of The current place to insert 
 * instructions and has methods to create new instructions. */
static IRBuilder<> Builder(getGlobalContext());

/* This are symbols tables for The code. The map name->object */
static std::map<std::string, AllocaInst*> NamedValues;
static std::map<std::string, Function*> Functions;

BasicBlock *ReturnBB = 0;
AllocaInst *RetAlloca = 0;

/* =================================================================
                            FUNCTIONS
 * ================================================================= */

void Error(const char *Str) { fprintf(stderr, "Error: %s\n", Str); }
Value *ErrorV(const char *Str) { Error(Str); exit(1); }

/* Returns true if the expression is boolean (result of a comparison) */
bool isComparable(cmodules::AstNodePtr expression){
  int kind = expression->eKind;
  return  kind == cmodules::EQ_EXP ||
          kind == cmodules::NE_EXP ||
          kind == cmodules::GT_EXP ||
          kind == cmodules::LT_EXP ||
          kind == cmodules::GE_EXP ||
          kind == cmodules::LE_EXP ;
}

/* Codegen The Allocas for local variables that correspond to The formal variables of function. 
 * function_node is an AST node representing a function. */
void CreateFormalVarAllocations(Function *F, cmodules::AstNodePtr formalVar) {

  for(Function::arg_iterator param = F->arg_begin(); formalVar != NULL; param++, formalVar = formalVar->sibling){
    
    Value *paramValue = param;
    std::string Name(strdup(formalVar->nSymbolPtr->id));
    cmodules::Type *type = formalVar->nSymbolPtr->stype;

    AllocaInst *allocaInst;
    if(type->kind == cmodules::INT)
      allocaInst = Builder.CreateAlloca(Type::getInt32Ty(getGlobalContext()), 0, Name);
    else
      allocaInst = Builder.CreateAlloca(Type::getInt32PtrTy(getGlobalContext()), 0, Name+".addr");

    allocaInst->setAlignment(4);
    Builder.CreateStore(paramValue, allocaInst)->setAlignment(4);
    // Map var -> llvm object
    NamedValues[Name] = allocaInst;
  }
}

/* Load the formalvar values into the already allocated registers and the return type */
Function* Codegen_Function_Declaration(cmodules::AstNodePtr declaration_node) {
  std::string Name(strdup(declaration_node->nSymbolPtr->id));
  std::vector<Type*> formalvars;
  cmodules::AstNodePtr formalVar; 

  // Arguments 
  for(formalVar = declaration_node->children[0]; formalVar != NULL; formalVar=formalVar->sibling){
    if(formalVar->nSymbolPtr->stype->kind == cmodules::INT)
      formalvars.push_back(Type::getInt32Ty(getGlobalContext()));
    else if(formalVar->nSymbolPtr->stype->kind == cmodules::ARRAY)
      formalvars.push_back(Type::getInt32PtrTy(getGlobalContext()));
    else ErrorV("Param type not supported");
  }

  // Return Type
  cmodules::Type* functionTypeList = declaration_node->nSymbolPtr->stype->function;
  FunctionType *FT;
  if(functionTypeList->kind==cmodules::INT)
     FT = FunctionType::get(Type::getInt32Ty(getGlobalContext()), formalvars, false);
  else if(functionTypeList->kind==cmodules::VOID)
    FT = FunctionType::get(Type::getVoidTy(getGlobalContext()), formalvars, false);
  else ErrorV("Return type not supported");

  // Set names for all arguments. Reuse formalVar
  Function *F = Function::Create(FT, Function::ExternalLinkage, Name, TheModule);
  formalVar = declaration_node->children[0];
  for (Function::arg_iterator AI = F->arg_begin(); formalVar != NULL; ++AI, formalVar=formalVar->sibling) {
          std::string argName(formalVar->nSymbolPtr->id);
          AI->setName(argName);
  }

  //add The Function to The map of functions
  Functions[Name] = F;
  return F;
}

//Generate code for expressions. needLoad is used to avoid the load of a lhs variable.
Value* Codegen_Expression(cmodules::AstNodePtr expression, bool needLoad = true) {

    switch(expression->eKind){
      case cmodules::ASSI_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0], false);
        Value *rhs = Codegen_Expression(expression->children[1]);
        StoreInst *store = Builder.CreateStore(rhs, lhs);
        store->setAlignment(4);
        
        // Return 1 in case the expression is the 'if' or the 'while' condition.
        return Builder.getInt32(1);
      }
      
      case cmodules::VAR_EXP: {
        std::string Name(strdup(expression->nSymbolPtr->id));
        
        AllocaInst *lVar = NamedValues[Name];
        GlobalVariable *gVar = TheModule->getNamedGlobal(Name);

        if(lVar){ // Local Variable
          if(needLoad){ // Has to be loaded (lhs)

            /* The variable is a local declared array (not an array parameter). It has been called by a function, 
             * so there are no square brakets and the parser thinks it's a normal variable. 
             * Example: int a[100]; sumElem(a); */
            if(expression->nSymbolPtr->stype->kind == cmodules::ARRAY && lVar->getAllocatedType()->isArrayTy()){
              Value *Args[] = { Builder.getInt32(0), Builder.getInt32(0) };
              return Builder.CreateInBoundsGEP(lVar, Args, "arraydecay");
            }

            // else
            LoadInst *load = Builder.CreateLoad(lVar);
            load->setAlignment(4);
            return load;
          }
          return lVar;
        } 
        else if(gVar){ // Global Variable
          if(needLoad){ // Has to be loaded (lhs)

            /* The variable is a local declared array (not an array parameter). It has been called by a function, 
             * so there are no square brakets and the parser thinks it's a normal variable. 
             * Example: int a[100]; sumElem(a); */
            if(expression->nSymbolPtr->stype->kind == cmodules::ARRAY){
              Value *Args[] = { Builder.getInt32(0), Builder.getInt32(0) };
              return Builder.CreateInBoundsGEP(gVar, Args, "arraydecay");
            }

            // else
            LoadInst *load = Builder.CreateLoad(gVar);
            load->setAlignment(4);
            return load;
          }
          return gVar;
        }
      }

      case cmodules::ARRAY_EXP: {
        std::string Name(strdup(expression->nSymbolPtr->id));
        Value *indexVal = Codegen_Expression(expression->children[0]);
        Value *Args[] = { Builder.getInt32(0), indexVal };

        AllocaInst *lVar = NamedValues[Name];
        GlobalVariable *gVar = TheModule->getNamedGlobal(Name); 

        Value *arrayElem;
        if(lVar){
          /* - Pointer Type are the formal parameters 
           * - Array Types are the local arrays declared with a dimension */
          if(lVar->getAllocatedType()->isArrayTy())
            arrayElem = Builder.CreateInBoundsGEP(lVar, Args, "arrayidx");
          else{
            LoadInst *loadedArrayPtr = Builder.CreateLoad(lVar);
            loadedArrayPtr->setAlignment(4);
            arrayElem = Builder.CreateInBoundsGEP(Builder.getInt32Ty(), loadedArrayPtr, indexVal, "arrayidx");
          }
        }
        // Must be a gvar
        else if(gVar)
          arrayElem = Builder.CreateInBoundsGEP(gVar, Args, "arrayidx");

        if(needLoad){ // Has to be loaded (lhs)
          LoadInst *load = Builder.CreateLoad(arrayElem);
          load->setAlignment(4);
          return load;
        } 
        return arrayElem;
      }
      
      case cmodules::CONST_EXP: {
        return Builder.getInt32(expression->nValue);
      }
      
      case cmodules::GT_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpSGT(lhs, rhs, "cmp");
      }
      
      case cmodules::LT_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpSLT(lhs, rhs, "cmp");
      }
      
      case cmodules::GE_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpSGE(lhs, rhs, "cmp");
      }
      
      case cmodules::LE_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpSLE(lhs, rhs, "cmp");
      }
      
      case cmodules::EQ_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpEQ(lhs, rhs, "cmp");
      }
      
      case cmodules::NE_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateICmpNE(lhs, rhs, "cmp"); 
      }
      
      case cmodules::ADD_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateAdd(lhs, rhs, "add");
      }
      
      case cmodules::SUB_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateSub(lhs, rhs, "sub");
      }
      
      case cmodules::MULT_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateMul(lhs, rhs, "mul"); 
      }
      
      case cmodules::DIV_EXP: {
        Value *lhs = Codegen_Expression(expression->children[0]);
        Value *rhs = Codegen_Expression(expression->children[1]);
        return Builder.CreateSDiv(lhs, rhs, "div"); 
      }
      
      case cmodules::CALL_EXP: {
        std::string Name(strdup(expression->fname));
        std::vector<Value*> args;
        cmodules::AstNodePtr currArg;

        Function *F = TheModule->getFunction(Name);

        for(currArg = expression->children[0]; currArg != NULL; currArg=currArg->sibling){
          Value *argVal = Codegen_Expression(currArg);
          args.push_back(argVal);
        }

        // If the function returns VOID
        if(F->getReturnType()->isVoidTy())
          return Builder.CreateCall(F, args);

        // else is INT
        return Builder.CreateCall(F, args, "call");
      }

      case cmodules::SEMI_EXP:
        return NULL;

      default: ErrorV("Expression not identified");
    }

    return NULL;
}

// Generates code for The statements.
Value* Codegen_Statement(cmodules::AstNodePtr statement, cmodules::Type* retType, int scopeDepth) {

    switch(statement->sKind){
      case cmodules::EXPRESSION_STMT: {
        return Codegen_Expression(statement->children[0]);
      }
      
      case cmodules::RETURN_STMT: {      
        // VOID RETURN TYPE 
        if(retType->kind == cmodules::VOID){
          // If scope depth 0 return the void
          if(scopeDepth == 0)
            return Builder.CreateRetVoid();
          else
          { 
            // If a return block hasnt been created, create it and branch, else branch
            if(ReturnBB == 0)
                ReturnBB = BasicBlock::Create(getGlobalContext(),"return");
        
            Builder.CreateBr(ReturnBB);
            return NULL;
          }

        // INT RETURN TYPE        
        } else if (retType->kind == cmodules::INT){
            if(scopeDepth == 0)
              return Builder.CreateRet(Codegen_Expression(statement->children[0]));

            // ELSE: If a return block hasnt been created yet, create it
            if(ReturnBB == 0)
              ReturnBB = BasicBlock::Create(getGlobalContext(),"return");

            // Allocate space, and store there the res
            if(RetAlloca == 0)
              RetAlloca = Builder.CreateAlloca(Builder.getInt32Ty(), 0, "retval");
            
            // Save there the return value
            Builder.CreateStore(Codegen_Expression(statement->children[0]), RetAlloca);

            // Jump to return branch
            Builder.CreateBr(ReturnBB);
            return NULL;
        }
      }
      
      case cmodules::IF_THEN_ELSE_STMT: { 
        scopeDepth++;
        Function *TheFunction = Builder.GetInsertBlock()->getParent();
        Value* condition = Codegen_Expression(statement->children[0]);

        // IF THEN ELSE
        if(statement->children[2])
        {
          BasicBlock *ThenBB = BasicBlock::Create(getGlobalContext(), "if.then", TheFunction);
          BasicBlock *ElseBB = BasicBlock::Create(getGlobalContext(), "if.else");
          BasicBlock *MergeBB = BasicBlock::Create(getGlobalContext(), "if.end");

          // To transform a constant or variable into boolean
          if( ! isComparable(statement->children[0]))
            condition = Builder.CreateICmpNE(condition, Builder.getInt32(0), "tobool");

          Builder.CreateCondBr(condition, ThenBB, ElseBB);
          Builder.SetInsertPoint(ThenBB);
          Value *ThenV = Codegen_Statement(statement->children[1],retType, scopeDepth); //generate code for the then part
          Builder.CreateBr(MergeBB); //after generating code, create a branch to the Merge basic block
          ThenBB = Builder.GetInsertBlock();
          TheFunction->getBasicBlockList().push_back(ElseBB); //add the Else basic block to the list of basic blocks of the function
          Builder.SetInsertPoint(ElseBB);
          Value *ElseV = Codegen_Statement(statement->children[2], retType, scopeDepth);
          Builder.CreateBr(MergeBB); //after generating code, create a branch to the Merge basic block
          TheFunction->getBasicBlockList().push_back(MergeBB);//add the Merge basic block to the list of basic blocks of the function
          Builder.SetInsertPoint(MergeBB);
        }
        // IF THEN
        else
        {
          BasicBlock *ThenBB = BasicBlock::Create(getGlobalContext(), "if.then", TheFunction);
          BasicBlock *MergeBB = BasicBlock::Create(getGlobalContext(), "if.end");

          // To transform a constant or variable into boolean
          if( ! isComparable(statement->children[0]))
            condition = Builder.CreateICmpNE(condition, Builder.getInt32(0),"tobool");

          Builder.CreateCondBr(condition, ThenBB, MergeBB);
          Builder.SetInsertPoint(ThenBB);
          Value *ThenV = Codegen_Statement(statement->children[1], retType, scopeDepth); //generate code the the then part
          Builder.CreateBr(MergeBB); //after generating code, create a branch to Merge basic block
          ThenBB = Builder.GetInsertBlock();
          TheFunction->getBasicBlockList().push_back(MergeBB);//add the Merge basic block to the list of basic blocks of the function
          Builder.SetInsertPoint(MergeBB);
        }

        scopeDepth--;
        return NULL;
      }
      
      case cmodules::COMPOUND_STMT: {
        cmodules::AstNodePtr currStmt;
        for(currStmt = statement->children[0] ; currStmt != NULL; currStmt = currStmt->sibling){
          scopeDepth++;
          Codegen_Statement(currStmt, retType, scopeDepth);
          scopeDepth--;
        }
        return NULL;
      }
      
      case cmodules::WHILE_STMT: {
        scopeDepth++;
        Function *TheFunction = Builder.GetInsertBlock()->getParent();

        // Create the three basic blocks
        BasicBlock *WhileBB = BasicBlock::Create(getGlobalContext(), "while.cond",TheFunction);
        BasicBlock *WhileBody = BasicBlock::Create(getGlobalContext(),"while.body");
        BasicBlock *WhileMerge = BasicBlock::Create(getGlobalContext(),"while.end");

        Builder.CreateBr(WhileBB);
        Builder.SetInsertPoint(WhileBB);

        // Get value of the condition
        Value* condition = Codegen_Expression(statement->children[0]);

        // To transform a constant or variable into boolean
        if( ! isComparable(statement->children[0]))
          condition = Builder.CreateICmpNE(condition, Builder.getInt32(0), "tobool");

        Builder.CreateCondBr(condition,WhileBody,WhileMerge);

        TheFunction->getBasicBlockList().push_back(WhileBody);
        Builder.SetInsertPoint(WhileBody);

        Value *WhileBodyValue = Codegen_Statement(statement->children[1], retType, scopeDepth);
        Builder.CreateBr(WhileBB);
        TheFunction->getBasicBlockList().push_back(WhileMerge); //add the Merge basic block to the list of basic blocks of the function
        Builder.SetInsertPoint(WhileMerge);
        
        scopeDepth--;
        return NULL;
      }

      default: ErrorV("Statement not identified");
    }

    return NULL;
}

//generates The code for The body of The function. Steps
//1. Generate alloca instructions for The local variables
//2. Iterate over list of statements and call Codegen_Statement for each of Them
Value* Codegen_Function_Body(cmodules::AstNodePtr function_node) {

  cmodules::AstNodePtr function_body = function_node->children[1];
  cmodules::ElementPtr firstLocalVar = function_body->nSymbolTabPtr->queue; 
  cmodules::ElementPtr localVar;
  for(localVar = firstLocalVar ; localVar != NULL ; localVar = localVar->queue_next){
    std::string Name(strdup(localVar->id));

    // To exclude the parameters (already allocated at this point)
    if( ! NamedValues[Name]){

      // INT variables
      if(localVar->stype->kind == cmodules::INT){
        IntegerType *intType = Builder.getInt32Ty(); //get an Integer type
        AllocaInst *allocaInst = Builder.CreateAlloca(intType, 0, Name);
        allocaInst->setAlignment(4);
        NamedValues[Name] = allocaInst;
      } 
      else

      // ARRAY variables
      if(localVar->stype->kind == cmodules::ARRAY){
        int arrayDimension = localVar->stype->dimension;
        ArrayType *arrType = ArrayType::get(Builder.getInt32Ty(), arrayDimension); //get an array (of integers) type
        AllocaInst *allocaInst = Builder.CreateAlloca(arrType, 0, Name);
        allocaInst->setAlignment(4);
        NamedValues[Name] = allocaInst;
      }
    }
  }

  cmodules::AstNodePtr firstStatement = function_body->children[0];
  cmodules::AstNodePtr statement;
  Value *retValue = NULL;
  for(statement = firstStatement ; statement != NULL; statement=statement->sibling)
    retValue = Codegen_Statement(statement, function_node->nSymbolPtr->stype->function, 0);
  
  return retValue;
}

/* Generates code for The function, verifies The code for The function. */
Function* Codegen_Function(cmodules::AstNodePtr function_node) {
  // Retrieve The pointer to The function 
  std::string Name(strdup(function_node->nSymbolPtr->id));
  Function *TheFunction = Functions[Name];

  //Create The entry block, place The insert point for The builder in The entry block
  BasicBlock *BB = BasicBlock::Create(getGlobalContext(), "entry", TheFunction);
  Builder.SetInsertPoint(BB);

  //Call CreateFormalVarAllocations
  CreateFormalVarAllocations(TheFunction, function_node->children[0]);

  //Call Codegen_Function_Body
  Value* lastStatement = Codegen_Function_Body(function_node);

  // Handle return statement
  if(ReturnBB != 0){
    TheFunction->getBasicBlockList().push_back(ReturnBB);
    if(function_node->nSymbolPtr->stype->function->kind == cmodules::VOID){
      Builder.SetInsertPoint(ReturnBB);
      Builder.CreateRetVoid();
    }
    else if(function_node->nSymbolPtr->stype->function->kind == cmodules::INT){
      Builder.SetInsertPoint(ReturnBB);
      Value* retVal = Builder.CreateLoad(RetAlloca,"rVal");
      Builder.CreateRet(retVal);
    }
  }

  // Verify and return the function
  verifyFunction(*TheFunction);
  return TheFunction;
}

/* Pass over The AST and declare The prototype of each function. This will allow forward calls. */
void initializeFunctions(cmodules::AstNodePtr program) {
    cmodules::AstNodePtr currentFun;
    for(currentFun = program ; currentFun != NULL; currentFun=currentFun->sibling)
       Function *TheFunction = Codegen_Function_Declaration(currentFun);
}

void codegen(char *inputFileName, char* outputFileName) {
  InitializeNativeTarget();
  LLVMContext &Context = getGlobalContext();
  // Make The module, which holds all The code.
  TheModule = new Module(inputFileName, Context);

  // Codegen The GLOBAL VARIABLES
  cmodules::ElementPtr currVar;
  for (currVar = symbolStackTop->symbolTablePtr->queue; currVar != NULL; currVar = currVar->queue_next){
    if (currVar->stype->kind==cmodules::INT) {
       /* INT VARIABLES */
       char *varName = strdup(currVar->id);

       // Creates The int variable, initialized to 0 and aligned 4
       TheModule->getOrInsertGlobal(varName, Builder.getInt32Ty());
       GlobalVariable* var = TheModule->getNamedGlobal(varName);
       var->setInitializer(Builder.getInt32(0));
       var->setAlignment(4);
       var->setLinkage(GlobalValue::CommonLinkage);
    }
    else if(currVar->stype->kind==cmodules::ARRAY) {
	     /* ARRAY VARIABLES*/
      char *varName = strdup(currVar->id);
      int arrayDimension = currVar->stype->dimension;
      ArrayType *arrType = ArrayType::get(Builder.getInt32Ty(), arrayDimension); //get an array (of integers) type

      // Creates The array variable, fully initialized to 0 and aligned 16
      TheModule->getOrInsertGlobal(varName, arrType);
      GlobalVariable* var = TheModule->getNamedGlobal(varName);
      var->setInitializer(ConstantAggregateZero::get(arrType));
      var->setAlignment(4);
      var->setLinkage(GlobalValue::CommonLinkage);
    }
  }
  
  // Pass over The AST and declare the prototype of each function. This will allow forward calls.
  initializeFunctions(program);

  // Generate the code for each function
  cmodules::AstNodePtr currentFunction;
  for(currentFunction = program ; currentFunction != NULL ; currentFunction = currentFunction->sibling){
    Function* TheFunction = Codegen_Function(currentFunction);
    // Clear the map and reset global variables
    NamedValues.clear();
    ReturnBB = 0;
    RetAlloca = 0;
  }

  // Print out all of the generated code. The default path and file name are #define at line 22
  std::error_code ErrInfo;
  char *outputFilePath = (char*)malloc(sizeof(char)*(strlen(OUTPUT_FILE_PATH)+strlen(outputFileName)+1));
  strcpy(outputFilePath, OUTPUT_FILE_PATH);
  strcat(outputFilePath, outputFileName);
  llvm::raw_ostream* filestream = new llvm::raw_fd_ostream(outputFilePath, ErrInfo, llvm::sys::fs::F_None);
  TheModule->print(*filestream, 0);
}

/* =================================================================
                            MAIN
 * ================================================================= */
/* Returns The name of The .ll*/
char *getOutputFileName(int argc, char** argv){
    char *filename;
    if(argc > 2)
      filename = strdup(argv[2]);
    else{
      filename = (char*)malloc(sizeof(char)*(strlen(DEFAULT_OUTPUT_FILE_NAME)+1));
      strcpy(filename,DEFAULT_OUTPUT_FILE_NAME);
    }
    return filename;
}

/* Returns The name of The module*/
char *getInputFileName(int argc, char** argv){
    char *filename;
    if(argc > 1)
      filename = strdup(argv[1]);
    else{
      filename = (char*)malloc(sizeof(char)*(strlen("stdin")+1));
      strcpy(filename,"stdin");
    }
    return filename;
}

int main(int argc, char** argv) {
    initLex(argc, argv); 
    yyparse();
    typecheck();
    codegen(getInputFileName(argc, argv), getOutputFileName(argc, argv));
    return 0;
}
