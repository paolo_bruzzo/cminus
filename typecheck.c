#include "symbolTable.h"
#include "ast.h"
#include "typecheck.h"

extern AstNodePtr program;

// Starts typechecking the AST  returns 1 on success
// use the global variable program
int typecheck(){
	AstNode *tmp;
	for(tmp=program; tmp!= NULL; tmp=tmp->sibling)
		if(!typecheck_method(tmp))
			return FALSE;
	return TRUE;
}

// compares two types and returns the resulting type
// from the comparison
Type* type_equiv(Type *t1, Type *t2){

	if(t1->kind == t2->kind)
		return newType(t1->kind);

	return NULL;
}

// Typechecks a method and returns 1 on success
int typecheck_method(AstNode *node){

	Type *retType = node->nSymbolPtr->stype->function;
	AstNode *stmt = node->children[1]; // the statements

	if( ! stmt->children[0])
		return TRUE;

	if(stmt->nKind == STMT){
		AstNode *tmp;
		for(tmp=stmt->children[0]; tmp->sibling != NULL; tmp=tmp->sibling)
			typecheck_stmt(tmp, retType);
		
		if(tmp->sKind != RETURN_STMT)
			error(tmp->nLinenumber,"Missing 'return' at the end of the method");
		
		typecheck_stmt(tmp,retType);
	}

	return TRUE;
}

// Typechecks a statement and returns 1 on success
int typecheck_stmt(AstNode *node, Type *retType){
	switch(node->sKind){
		case EXPRESSION_STMT:{
			AstNode *expr = node->children[0];
			if(typecheck_expr(expr) != NULL)
				return TRUE;
			error(node->nLinenumber,"Expression type mismatch");
		}
		case RETURN_STMT:{
			AstNode *expr = node->children[0];
			if(expr == NULL && retType->kind == VOID)
				return TRUE;
			if(expr != NULL)
				if(type_equiv(retType, typecheck_expr(expr)) != NULL)
					return TRUE;
			error(node->nLinenumber,"Wrong return type");
		}
		case COMPOUND_STMT:{
			AstNode *expr = node->children[0];
			AstNode *tmp;
			for(tmp=expr; tmp!=NULL; tmp=tmp->sibling)
				if(typecheck_stmt(tmp, retType) == FALSE)
					error(node->nLinenumber,"Something wrong with this coumpond stmt");
			return TRUE;
		}
		case WHILE_STMT:{
			AstNode *condExpr = node->children[0];
			AstNode *whileExpr = node->children[1];
			Type *condExprType = typecheck_expr(condExpr); 
			if(condExprType->kind != INT)
				error(condExpr->nLinenumber,"The 'while' condition must be an integer expression");
			if(typecheck_stmt(whileExpr, retType) == TRUE)
				return TRUE;
			error(node->nLinenumber,"Wrong 'while' statement");
		}
		case IF_THEN_ELSE_STMT:{
			AstNode *condExpr = node->children[0];
			AstNode *thenStmt = node->children[1];
			AstNode *elseStmt = node->children[2];
			Type *condExprType = typecheck_expr(condExpr); 
			if(condExprType->kind != INT)
				error(condExpr->nLinenumber,"The 'if' condition must be an integer expression");
			if(typecheck_stmt(thenStmt, retType) != FALSE){
				if(elseStmt != NULL){
					if(typecheck_stmt(elseStmt, retType) != FALSE)
						return TRUE;
				}
				return TRUE;
			}
			error(node->nLinenumber,"Wrong 'if' statement");

		}
		default: error(node->nLinenumber,"Unrecongnized statement");
	}
	return FALSE;
}

// Type checks a given expression and returns its type
Type* typecheck_expr(AstNode *node){

	switch(node->eKind){
		case VAR_EXP:{
			if(node->nSymbolPtr->stype->kind == INT || node->nSymbolPtr->stype->kind == ARRAY) 
				return node->nSymbolPtr->stype;
			char mex[100];
			sprintf(mex,"variable '%s' has been referenced with the wrong type",node->nSymbolPtr->id);
			error(node->nLinenumber, mex);
		} 

		case ARRAY_EXP:{
			if(node->nSymbolPtr->stype->kind == ARRAY){
				Type *indexType = typecheck_expr(node->children[0]);
				if(indexType->kind == INT)
					return newType(INT);
			}
			char mex[100];
			sprintf(mex,"variable '%s' has been referenced with the wrong type",node->nSymbolPtr->id);
			error(node->nLinenumber, mex);
		}

		case ADD_EXP:
		case SUB_EXP:
		case MULT_EXP:
		case DIV_EXP:
		case GT_EXP:
		case LT_EXP:
		case GE_EXP:
		case LE_EXP:{
			//printf("expression check: OPERATION\n");
			Type* t1 = typecheck_expr(node->children[0]);
			Type* t2 = typecheck_expr(node->children[1]);
			if(t1->kind == INT && t2->kind == INT)
				return type_equiv(t1, t2);

			error(node->nLinenumber,"Binary expression type mismatch, only 'int' types allowed");
		}

		case EQ_EXP:
		case NE_EXP:{
			//printf("expression check: EQ / NE\n");
			Type* t1 = typecheck_expr(node->children[0]);
			Type* t2 = typecheck_expr(node->children[1]);
			if(type_equiv(t1, t2))
				return newType(INT);

			error(node->nLinenumber,"The left value is different from the right one");
		}

		case CONST_EXP:
			return newType(INT);

		case ASSI_EXP:{
			//printf("expression check: ASSIGN\n");
			AstNode *left = node->children[0];
			AstNode *right = node->children[1];
			if((left->eKind == VAR_EXP && left->nSymbolPtr->stype->kind == INT) || left->eKind == ARRAY_EXP)
				return type_equiv(typecheck_expr(left), typecheck_expr(right));
			
			error(node->nLinenumber,"Left expression on assignment is not an l-value");
		}

		case CALL_EXP:{
			// Symbol table entry
			ElementPtr el = symLookup(node->fname);
			if(!el){
				char mex[100];
				sprintf(mex,"Function '%s()' has never been defined",node->fname);
				error(node->nLinenumber,mex);
			}
			
			// Ast of the declaration
			AstNode *decl = el->snode;
			AstNode *tmpDecl, *tmpCalled;

			int paramNumeberDeclared = 0;
			// Iterate on the ast declaration
			for(tmpDecl=decl->children[0]; tmpDecl!=NULL; tmpDecl=tmpDecl->sibling)
				paramNumeberDeclared++;

			int paramNumberCalled = 0;
			// Iterate on the call
			for(tmpCalled=node->children[0]; tmpCalled!=NULL ; tmpCalled=tmpCalled->sibling)
				paramNumberCalled++;
			

			// If the number of called parameters is different
			if(paramNumeberDeclared != paramNumberCalled)
				error(node->nLinenumber,"Number of parameters doesn't match with the declaration");

			// If there are no parameters
			if(paramNumeberDeclared == 0)
				return newType(el->stype->function->kind);

			int i;
			for(tmpCalled = node->children[0], tmpDecl = decl->children[0] ; tmpDecl != NULL ; tmpDecl=tmpDecl->sibling, tmpCalled=tmpCalled->sibling)
				if(type_equiv(typecheck_expr(tmpCalled), typecheck_expr(tmpDecl)) == NULL)
					error(node->nLinenumber,"Some parameter type does not match");
			
			// Return the return type of the called function
			return newType(el->stype->function->kind);
		}
		case SEMI_EXP:
			return newType(VOID);
	}

	// Should never get here
	return NULL;
}


// Print the error and exit
void error (int line, char const *s) {
	if(line != 0)
       	fprintf (stderr, "Line %d: %s\n", line, s);
   	else
   		fprintf (stderr, "%s\n", s);
    exit(1);
}

Type* newType(int kind){
	Type *type = (Type*) malloc(sizeof(Type));
	type->function = NULL;
	type->kind = kind;
	return type;
}


