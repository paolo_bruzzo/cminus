#include "symbolTable.h"

// Top should point to the top of the scope stack, which is the most recent scope pushed
SymbolTableStackEntryPtr symbolStackTop;

// Keeps the current scope depth (0 is the main scope)
int scopeDepth;

/* =======================================
 *       Global Functions Prototypes 
 * ======================================= */

/* Allocate the global scope entry and symbol table --and set scopeDepth to 0. */
void initSymbolTable(){
	// Allocate memory for the first scope (main)
	symbolStackTop = (SymbolTableStackEntryPtr) malloc(sizeof(struct symbolTableStackEntry));
	if(symbolStackTop == NULL)
		return ;

	// Allocate memory for the symbol table pointer inside the first scope
	symbolStackTop->symbolTablePtr = (SymbolTablePtr) malloc(sizeof(struct symbolTable));
	if(symbolStackTop->symbolTablePtr == NULL)
		return ;

	int i;
	// No elements in the symbol table
	for(i=0; i<MAXHASHSIZE; i++)
		symbolStackTop->symbolTablePtr->hashTable[i] = NULL;

	// No previous scope for the first scope
	symbolStackTop->prevScope = NULL;

	// No first elements in the simbol table
	symbolStackTop->symbolTablePtr->queue = NULL;

	// depth of the first scope
	scopeDepth = 0;
}

/* Computes the hash of the element */
int hash(char *id) {
	int i;
	int sum = 0;
	int len = strlen(id);

	for(i=0 ; i<len ; i++)
		sum += (id[i]+i*i);

	return sum % MAXHASHSIZE;
}

// Look up a given entry
ElementPtr symLookup(char *name){
	int elemHash = hash(name);
	SymbolTableStackEntryPtr currScope;
	// Iterate over the scopes
	for(currScope=symbolStackTop; currScope != NULL; currScope=currScope->prevScope){
		ElementPtr hashTableCell = currScope->symbolTablePtr->hashTable[elemHash];
		ElementPtr currElem;
		// Iterate over the element(s), some of them may have returned the same hash value
		for(currElem=hashTableCell; currElem != NULL ; currElem=currElem->next)
			if(strcmp(currElem->id,name)==0)
				return currElem;
	}
	return NULL;
}


// Insert an element with a specified type in a particular line number
// initialize the scope depth of the entry from the global var scopeDepth
ElementPtr symInsert(char *name, TypePtr type, int line){

	int elemHash = hash(name);

	// Create new element
	ElementPtr newElem = (ElementPtr) malloc (sizeof(Element));
	if(newElem == NULL)
		return NULL;

	// Init new element
	newElem->id = strdup(name);
	newElem->stype = type;
	newElem->linenumber = line;
	newElem->scope = scopeDepth;
	newElem->key = elemHash;
	newElem->snode = NULL;
	newElem->queue_next = NULL;

	// Set the queue pointers
	if(symbolStackTop->symbolTablePtr->queue == NULL)
		symbolStackTop->symbolTablePtr->queue = newElem;
	else{
		ElementPtr tmp = symbolStackTop->symbolTablePtr->queue;
		while(tmp->queue_next != NULL)
			tmp = tmp->queue_next;
		tmp->queue_next = newElem;
	}

	// Insert at the beginning
	newElem->next = symbolStackTop->symbolTablePtr->hashTable[elemHash] ;
	symbolStackTop->symbolTablePtr->hashTable[elemHash] = newElem ;

	return newElem;
}

/* Push a new entry to the symbol stack. */
void enterScope(){

	// Allocate memory for the new scope
	SymbolTableStackEntryPtr newScope = (SymbolTableStackEntryPtr) malloc(sizeof(struct symbolTableStackEntry));
	if(newScope == NULL)
		return;

	// Allocate memory for the symbol table
	newScope->symbolTablePtr = (SymbolTablePtr) malloc(sizeof(struct symbolTable));
	if(newScope->symbolTablePtr == NULL)
		return;

	int i;
	// No elements in the symbol table
	for(i=0; i<MAXHASHSIZE; i++)
		newScope->symbolTablePtr->hashTable[i] = NULL;

	newScope->symbolTablePtr->queue = NULL;

	// Make the newscope point to the previous one
	newScope->prevScope = symbolStackTop;
	symbolStackTop = newScope;

	// Increment scope Depth
	scopeDepth ++;
}


/* Pop an entry off the symbol stack
 * This should modify top and change scope depth */
void leaveScope(){

	// Memorize the old scope
	SymbolTableStackEntryPtr oldScope = symbolStackTop;

	// make the top of the stack point to the previous one
	symbolStackTop = oldScope->prevScope;

	// decrement scope depth
	scopeDepth--;

	// free old scope memory
	free(oldScope);
}


/* Print the element number */
void printElement(ElementPtr symelement) {
	if (symelement != NULL) {
		printf("Line %d: %s\n", symelement->linenumber, symelement->id);
	}
	else printf("Wrong call! symbol table entry NULL\n");
}


/*should traverse through the entire symbol table and print it
 * must use the printElement function given above */
void printSymbolTable(){

	SymbolTableStackEntryPtr currScope;

	for(currScope = symbolStackTop; currScope != NULL ; currScope=currScope->prevScope){		
		ElementPtr currElem;
		for(currElem=currScope->symbolTablePtr->queue; currElem != NULL; currElem=currElem->queue_next)
			printElement(currElem);
	}
}












