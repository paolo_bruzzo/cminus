#include "symbolTable.h"
#include "ast.h"

extern SymbolTableStackEntryPtr symbolStackTop;

extern int scopeDepth;
extern int yylineno;

//creates a new expression node
AstNodePtr new_ExprNode(ExpKind kind){
	AstNodePtr new_node = new_Node(EXPRESSION);
	new_node->eKind=kind;
	return new_node;
}

//creates a new statement node
AstNodePtr new_StmtNode(StmtKind kind){
	AstNodePtr new_node = new_Node(STMT);
	new_node->sKind=kind;
	return new_node;
}

//creates a new formalvar/method node
AstNodePtr new_Node(NodeKind nKind){
	AstNodePtr new_node = (AstNodePtr) malloc(sizeof(AstNode));
	new_node->nKind = nKind;
	new_node->sibling = NULL;
	int i;
	for(i=0 ; i < MAXCHILDREN ; i++)
		new_node->children[i] = NULL;
	new_node->nType = NULL;
	new_node->nSymbolPtr = NULL;
	new_node->nSymbolTabPtr = NULL;
	new_node->fname = NULL;
	new_node->nLinenumber = yylineno;
	return new_node;
}

//creates a new type node for entry into symbol table
Type* new_type(TypeKind kind){
	Type* ty = (Type*)malloc(sizeof(Type));
	ty->kind=kind;
	ty->function=NULL;
	return ty;
}