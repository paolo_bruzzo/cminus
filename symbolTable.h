
#ifndef _SYMBOL_TABLE
#define _SYMBOL_TABLE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "globals.h"

/* ==== TYPE STRUCT ===== */
typedef enum  {VOID, INT, ARRAY, FUNCTION} TypeKind;

typedef struct type{
	TypeKind 	kind;   	/* one from the enum above */
	int  		dimension; 	/* for arrays */
	struct type *function; 	/*function argument and return types */
}Type;

typedef Type *TypePtr;


/* ==== ELEMENTS OF THE SYMBOL TABLE ===== */        
typedef struct element {
	int				key;
    char 			*id;	       
	int   	      	linenumber;
	int         	scope;   	/* scope depth at declaration */
	Type     		*stype;	    /* pointer to the type infomation */
	struct element	*next;		/* pointer to the next symbol with the same hash table index */
    struct node     *snode;   	/* AST Node for method declartion and formal vars */
	struct element  *queue_next;/* Pointer to next element in  queue of  declarared elements  */
} Element;

typedef Element	*ElementPtr;
typedef ElementPtr HashTableEntry;


/* ==== SYMBOL TABLE ===== */
typedef struct symbolTable {
	HashTableEntry	hashTable[MAXHASHSIZE];	/* hash table  */
	struct element 	*queue; 				/* points to first inserted (or first declared) symbol */
} SymbolTable;

typedef SymbolTable	*SymbolTablePtr;


/* ==== SCOPES STRUCT ===== */
typedef struct symbolTableStackEntry {
	SymbolTablePtr					symbolTablePtr;
	struct symbolTableStackEntry	*prevScope;
} SymbolTableStackEntry;

typedef SymbolTableStackEntry		*SymbolTableStackEntryPtr;


/* ==== FUNCTIONS ===== */
void			initSymbolTable() ;
ElementPtr		symLookup(char *);
ElementPtr		symInsert(char *, Type *, int );
void			enterScope();
void			leaveScope();
void			printSymbolTable();

#endif
