%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ast.h"
#include "symbolTable.h"
#include "util.h"
#include "typecheck.h"

/* external function prototypes */
extern int yylex();
extern int initLex(int, char **); 

/* external global variables */

extern int	yydebug;
extern int	yylineno;
extern SymbolTableStackEntryPtr symbolStackTop;
extern int scopeDepth;

/* function prototypes */ 
void yyerror(const char *);
AstNodePtr lastSiblingOf(AstNodePtr);

/* global variables */

int scopeAlreadyPushed; 
AstNodePtr  program;
int yydebug = 0;

%}

/* YYSTYPE */
%union
{
    AstNodePtr nodePtr;
    int        iVal;
    char      *cVal;
    Type      *type;
}

%error-verbose

// TOKENS
%token			TOK_ELSE	
%token			TOK_IF	
%token			TOK_RETURN	
%token			TOK_VOID	
%token			TOK_INT	
%token			TOK_WHILE	
%token<cVal>	TOK_ID	
%token<iVal>	TOK_NUM	
%token			TOK_PLUS	
%token			TOK_MINUS	
%token			TOK_MULT	
%token			TOK_DIV	
%token			TOK_LT	
%token			TOK_LE	
%token			TOK_GT	
%token			TOK_GE	
%token			TOK_EQ	
%token			TOK_NE	
%token			TOK_ASSIGN	
%token			TOK_SEMI	
%token			TOK_COMMA	
%token			TOK_LPAREN	
%token			TOK_RPAREN	
%token			TOK_LSQ	
%token			TOK_RSQ	
%token			TOK_LBRACE	
%token			TOK_RBRACE	
%token			TOK_ERROR

// TYPES
%type <nodePtr> Fun_declaration_list Fun_declaration
%type <type> 	Type_specifier
%type <nodePtr> Compound_stmt Statement_list Statement
%type <nodePtr> Selection_stmt Iteration_stmt Return_stmt Param Params Param_list
%type <nodePtr> Expression_stmt Expression Simple_expression Additive_expression Factor Var Call
%type <nodePtr> Args Args_list
%type <nodePtr> Rel_op Add_op Mult_op Term

/* associativity and precedence */
/* specify operator precedence (taken care of by grammar) and associatity here --uncomment */


%nonassoc	"if_no_else"
%nonassoc	TOK_ELSE

%right 		TOK_ASSIGN
%left		TOK_NE
%left		TOK_EQ
%nonassoc	TOK_GE
%nonassoc	TOK_LE
%nonassoc	TOK_GT
%nonassoc	TOK_LT
%left		TOK_MINUS
%left		TOK_PLUS
%left		TOK_DIV
%left		TOK_MULT 

/* Begin your grammar specification here */
%%

Start					: {initSymbolTable();} Var_declaration_list Fun_declaration_list {
							program = $3;
						  } ;

Var_declaration_list	: Var_declaration_list Var_declaration 
						| /* epsilon */ ;

Fun_declaration_list 	: Fun_declaration_list Fun_declaration {
							AstNodePtr last = lastSiblingOf($1);
							last->sibling = $2;
							$$ = $1;
						  } 
						| Fun_declaration { 
							$$ = $1;
						  };

Var_declaration			: Type_specifier TOK_ID TOK_SEMI {
							if(scopeDepth >= 2)
								yyerror("Variables can only be declared globally or in the function scope");
							if($1->kind == VOID)
								yyerror("VOID type variables are not supported");
							ElementPtr el = symLookup($2);
							if(el == NULL || el->scope != scopeDepth)
								symInsert($2,$1,yylineno);
							else{
								char err_message[100];
								sprintf(err_message,"Variable '%s' already declared in this scope",$2);
								yyerror(err_message);
							}
						  } 
						| Type_specifier TOK_ID TOK_LSQ TOK_NUM TOK_RSQ TOK_SEMI {
							if(scopeDepth >= 2)
								yyerror("Variables can only be declared globally or in the function scope");
							if($1->kind == VOID)
								yyerror("VOID type arrays are not supported");
							ElementPtr el = symLookup($2);
							if(el == NULL || el->scope != scopeDepth){
								$1->kind = ARRAY;
								$1->dimension = $4;
								symInsert($2,$1,yylineno);
							}
							else{
								char err_message[100];
								sprintf(err_message,"Variable '%s' already declared in this scope",$2);
								yyerror(err_message);
							}
						  } ;

Fun_declaration			: Type_specifier TOK_ID TOK_LPAREN {
							ElementPtr el = symLookup($2);
							if(el == NULL){
								$<nodePtr>$ = new_Node(METHOD);
								$<nodePtr>$->nSymbolPtr = symInsert($2,$1,yylineno);
								$<nodePtr>$->nSymbolPtr->snode = $<nodePtr>$;
								$<nodePtr>$->nSymbolPtr->stype->function = (Type*) malloc(sizeof(Type));
								$<nodePtr>$->nSymbolPtr->stype->function->kind = $1->kind;
								$1->kind = FUNCTION;
								$<nodePtr>$->nType = $1;
								enterScope();
								//Used to notify the compound_stmt not to push
								scopeAlreadyPushed = TRUE;
							}
							else{
								char err_message[100];
								sprintf(err_message,"Function '%s' already declared in this scope",$2);
								yyerror(err_message);
							}
						  }
						  Params TOK_RPAREN Compound_stmt {
							/* $5->type = ; */
							$<nodePtr>4->children[0] = $5;
							$<nodePtr>4->children[1] = $7;
							$$ = $<nodePtr>4;
						  };

Type_specifier			: TOK_INT{
							$$ = new_type(INT);
						  }
						| TOK_VOID{
							$$ = new_type(VOID);
						  } ;

Params					: Param_list {
							$$ = $1;
						  }
						| TOK_VOID {
							$$ = NULL;
						  };

Param_list				: Param_list TOK_COMMA Param {
							AstNodePtr last = lastSiblingOf($1);
							last->sibling = $3;
							$$ = $1;
						  } 
						| Param {
						 	$$ = $1;
						  };

Param					: Type_specifier TOK_ID {
							if($1->kind == VOID)
								yyerror("VOID type variables are not supported");
							ElementPtr el = symLookup($2);
							if(el == NULL || el->scope != scopeDepth){
								$$ = new_Node(FORMALVAR);
								$$->nType = $1;
								$$->nSymbolPtr = symInsert($2,$1,yylineno);
								$$->nSymbolPtr->snode = $$;
							}
							else{
								char err_message[100];
								sprintf(err_message,"Parameter '%s' already declared in this function",$2);
								yyerror(err_message);
							}
						  }
						| Type_specifier TOK_ID TOK_LSQ TOK_RSQ {
							if($1->kind == VOID)
								yyerror("VOID type arrays are not supported");
							ElementPtr el = symLookup($2);
							if(el == NULL || el->scope != scopeDepth){
								$1->kind = ARRAY;
								$$ = new_Node(FORMALVAR);
								$$->nType = $1;
								$$->nSymbolPtr = symInsert($2,$1,yylineno);
								$$->nSymbolPtr->snode = $$;
							}
							else{
								char err_message[100];
								sprintf(err_message,"Parameter '%s' already declared in this function",$2);
								yyerror(err_message);
							}
						  };

Compound_stmt			: TOK_LBRACE {
							if(scopeAlreadyPushed == FALSE){
								enterScope();
							}
							else
								scopeAlreadyPushed = FALSE;
						  	} 
						  Local_declaration_list Statement_list TOK_RBRACE {
							$$ = new_StmtNode(COMPOUND_STMT);
							$$->children[0] = $4;
							$$->nSymbolTabPtr = symbolStackTop->symbolTablePtr;
							leaveScope();
						  };

Local_declaration_list 	: Local_declaration_list Local_declaration 
						| /* epsilon */ ;

Statement_list			: Statement_list Statement {
							if($1==NULL)
								$$ = $2;
							else{
								AstNodePtr last = lastSiblingOf($1);
								last->sibling = $2;
								$$ = $1;
							}
						  }
						| /* epsilon */ { 
						  	$$ = NULL;
						  } ;

Local_declaration		: Var_declaration ;

Statement				: Expression_stmt{
							$$ = $1;
						  } 
						| Compound_stmt{
							$$ = $1;
						  }
						| Selection_stmt{
							$$ = $1;
						  }
						| Iteration_stmt{
							$$ = $1;
						  } 
						| Return_stmt{
							$$ = $1;
						  } ;

Expression_stmt			: Expression TOK_SEMI {
							$$ = new_StmtNode(EXPRESSION_STMT);
							$$->children[0] = $1;
						  }
						| TOK_SEMI {
							$$ = new_StmtNode(EXPRESSION_STMT);
							$$->children[0] = new_ExprNode(SEMI_EXP);
						  };

Selection_stmt			: TOK_IF TOK_LPAREN Expression TOK_RPAREN Statement %prec "if_no_else" {
							$$ = new_StmtNode(IF_THEN_ELSE_STMT);
							$$->children[0] = $3;
							$$->children[1] = $5;
						  }
						| TOK_IF TOK_LPAREN Expression TOK_RPAREN Statement TOK_ELSE Statement {
							$$ = new_StmtNode(IF_THEN_ELSE_STMT);
							$$->children[0] = $3;
							$$->children[1] = $5;
							$$->children[2] = $7;
						  };

Iteration_stmt			: TOK_WHILE TOK_LPAREN Expression TOK_RPAREN Statement {
							$$ = new_StmtNode(WHILE_STMT);
							$$->children[0] = $3;
							$$->children[1] = $5;
						  };

Return_stmt				: TOK_RETURN TOK_SEMI {
							$$ = new_StmtNode(RETURN_STMT);
						  }
						| TOK_RETURN Expression TOK_SEMI {
							$$ = new_StmtNode(RETURN_STMT);
							$$->children[0] = $2;
						  };

Expression				: Var TOK_ASSIGN Expression{
							$$ = new_ExprNode(ASSI_EXP);
							$$->children[0] = $1;
							$$->children[1] = $3;
						  }
						| Simple_expression {
							$$ = $1;
						  };

Var						: TOK_ID {
							ElementPtr el = symLookup($1);
							if(el != NULL){
								$$ = new_ExprNode(VAR_EXP);
								$$->nSymbolPtr = el;
							}
							else{
								char err_message[100];
								sprintf(err_message,"Variable '%s' has never been declared",$1);
								yyerror(err_message);
							}
						  }
						| TOK_ID TOK_LSQ Simple_expression TOK_RSQ {
							ElementPtr el = symLookup($1);
							if(el != NULL){
								$$ = new_ExprNode(ARRAY_EXP);
								$$->nSymbolPtr = el;
								$$->children[0] = $3;
							}
							else{
								char err_message[100];
								sprintf(err_message,"Array '%s[]' has never been declared",$1);
								yyerror(err_message);
							}
						  };

Simple_expression		: Additive_expression Rel_op Additive_expression {
							$$ = $2;
							$$->children[0] = $1;
							$$->children[1] = $3;
						  }
						| Additive_expression {
							$$ = $1;
						  };

Rel_op					: TOK_EQ {
							$$ = new_ExprNode(EQ_EXP);
						  }
						| TOK_NE {
							$$ = new_ExprNode(NE_EXP);
						  }
						| TOK_GT {
							$$ = new_ExprNode(GT_EXP);
						  }
						| TOK_LT {
							$$ = new_ExprNode(LT_EXP);
						  }
						| TOK_GE {
							$$ = new_ExprNode(GE_EXP);
						  }
						| TOK_LE {
							$$ = new_ExprNode(LE_EXP);
						  };

Additive_expression		: Additive_expression Add_op Term {
							$$ = $2;
							$$->children[0] = $1;
							$$->children[1] = $3;
						  }
						| Term {
						 	$$ = $1;
						  };

Add_op					: TOK_PLUS {
							$$ = new_ExprNode(ADD_EXP);
						  }
						| TOK_MINUS {
							$$ = new_ExprNode(SUB_EXP);
						  };

Term					: Term Mult_op Factor{
							$$ = $2;
							$$->children[0] = $1;
							$$->children[1] = $3;
						  }
						| Factor {
							$$ = $1;
						  };

Mult_op					: TOK_MULT {
							$$ = new_ExprNode(MULT_EXP);
						  }
						| TOK_DIV {
							$$ = new_ExprNode(DIV_EXP);
						  };

Factor					: TOK_LPAREN Simple_expression TOK_RPAREN {
							$$ = $2;
						  }
						| Var{
							$$ = $1;
						  }
						| Call{
							$$ = $1;
						  }
						| TOK_NUM {
							$$ = new_ExprNode(CONST_EXP);
							$$->nValue = $1;
						  };

Call					: TOK_ID TOK_LPAREN Args TOK_RPAREN {
							$$ = new_ExprNode(CALL_EXP);
							$$->fname = strdup($1);
							$$->children[0] = $3;
						  };

Args					: Args_list {
							$$ = $1;
						  }
						| /* epsilon */ { 
						  	$$ = NULL;
						  };

Args_list				: Args_list TOK_COMMA Simple_expression {
							AstNodePtr last = lastSiblingOf($1);
							last->sibling = $3;
							$$ = $1;
						  }
						| Simple_expression {
							$$ = $1;
						  };

%%

AstNodePtr lastSiblingOf(AstNodePtr ptr){
	if(ptr==NULL)
		return ptr;
	while(ptr->sibling!=NULL)
		ptr=ptr->sibling;
	return ptr;
}

void yyerror (char const *s) {
       fprintf (stderr, "Line %d: %s\n", yylineno, s);
       exit(1);
}
