# README #
Front-end compiler for the Cminus language; it generates LLVM IR code starting from a Cminus file. To compile it, LLVM source code and build must be available on the machine itself.