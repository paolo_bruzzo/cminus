/**********************/
/* C header files */
/**********************/

%{
#include "globals.h"
#include "ast.h"
#include "cmparser.tab.h"
%}

%option yylineno

/**********************/
/* start your regular definitions  here */
/**********************/

LINENUMBER		\n
SPACE			[ \t\r]+
ERROR			byte|float|auto|break|case|char|const|continue|default|do|double|for|goto|long|short|register|switch|~|\?|:|\+\+|\-\-|!|&|\||\^|<<|>>|>>>|\+=|\-=|\*=|\/=|&=|\|=|\^=|%=|<<=|>>=|>>>=
COMMENT_MULTI	\/\*([^*]|\*+[^*\/])*\*+\/
COMMENT_LINE	\/\/.*
ELSE			else
IF				if
RETURN			return
VOID			void
INT				int
WHILE			while
ID				[a-zA-Z$_][0-9a-zA-Z$_]*
NUMBER			0|[1-9][0-9]*
ERROR_NUMBER    0+[0-9]+
PLUS			\+
MINUS			\-
MULT			\*
DIV				\/
LT				<
LE				<=
GT				>
GE				>=
EQ				==
NE				!=
ASSIGN			=
SEMI			;
COMMA			,
LPAREN			\(
RPAREN			\)
LSQ				\[
RSQ				\]
LBRACE			\{
RBRACE			\}
OTHER			.

/* start your token specifications here */
/* Token names must come from cmparser.tab.h */

%%
{LINENUMBER}	{ /*Ignore \n */ }
{SPACE}			{ /*Ignore whitespace and tab*/ }
{ERROR}			{ return TOK_ERROR; }
{COMMENT_MULTI}	{ /*Ignore comment multiline*/ }
{COMMENT_LINE}	{ /*Ignore comment on a single line*/ }
{ELSE}			{ return TOK_ELSE; }
{IF}			{ return TOK_IF; }
{RETURN}		{ return TOK_RETURN; }
{VOID}			{ return TOK_VOID; }
{INT}			{ return TOK_INT; }
{WHILE}			{ return TOK_WHILE; }
{ID}			{ yylval.cVal = strdup(yytext); return TOK_ID; }
{NUMBER} 		{ yylval.iVal = atoi(yytext); return TOK_NUM; }
{ERROR_NUMBER}  { return TOK_ERROR; }
{PLUS}			{ return TOK_PLUS; }
{MINUS}			{ return TOK_MINUS; }
{MULT}			{ return TOK_MULT; }
{DIV}			{ return TOK_DIV; }
{LT}			{ return TOK_LT; }
{LE}			{ return TOK_LE; }
{GT}			{ return TOK_GT; }
{GE}			{ return TOK_GE; }
{EQ}			{ return TOK_EQ; }
{NE}			{ return TOK_NE; }
{ASSIGN}		{ return TOK_ASSIGN; }
{SEMI}			{ return TOK_SEMI; }
{COMMA}			{ return TOK_COMMA; }
{LPAREN}		{ return TOK_LPAREN; }
{RPAREN}		{ return TOK_RPAREN; }
{LSQ}			{ return TOK_LSQ; }
{RSQ}			{ return TOK_RSQ; }
{LBRACE}		{ return TOK_LBRACE; }
{RBRACE}		{ return TOK_RBRACE; }
{OTHER}			{ return TOK_ERROR; }

%%
/**********************/
/* C support functions */
/**********************/

int initLex(int argc, char **argv){
    
   if ( argc > 1 )
      	yyin = fopen( argv[1], "r" );
   else
    	yyin = stdin;
   
   return 0;
} 
